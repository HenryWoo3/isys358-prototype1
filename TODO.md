GENERAL
	- Separate CSS and JS into separate files (or save this for the next prototype)
	- Add Nav bar to all html files
	- Add comments to all files

HOMEPAGE
	- Stylise about sections
	- Add fancy scroll thing

LOGIN
	- Stylise submit/login button

SIGNUP
	- Add and stylise form

DASHBOARD
	- Add post-login nav bar
	- Add and stylise actual dashboard

SETTINGS
	- Add post-login nav bar
	- Add and stylise settings menu

SEARCH
	- Add and stylise results

ADVANCED SEARCH
	- Add search form
